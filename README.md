# Demonstration

![Alt Text](./demonstration.gif)

# Instructions on running

just open index.html in your broswer from the folder created after unzipping

# What I would do with more time

1. make animations so that new games would appear and disappear gradually
2. make images for game tiles dynamically size based on screen width
3. If the list of games were very large, create a server to get games associated with a specific query, or
4. Move name of game in search suggestion to vertical center of image
